//
//  FontsListTableViewCell.h
//  FancyFonts
//
//  Created by Pradeep Rajkumar on 30/12/14.
//  Copyright (c) 2014 FancyFonts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FontsListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *fontNameLabel;

@end