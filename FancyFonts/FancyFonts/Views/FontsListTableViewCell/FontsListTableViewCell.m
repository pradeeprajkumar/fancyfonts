//
//  FontsListTableViewCell.m
//  FancyFonts
//
//  Created by Pradeep Rajkumar on 30/12/14.
//  Copyright (c) 2014 FancyFonts. All rights reserved.
//

#import "FontsListTableViewCell.h"

@implementation FontsListTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
