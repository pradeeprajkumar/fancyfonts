//
//  FancyFontsTextView.h
//  FancyFonts
//
//  Created by Pradeep Rajkumar on 30/12/14.
//  Copyright (c) 2014 FancyFonts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FancyFontsTextView : UITextView

@property (strong, nonatomic) NSArray *availableFontsArray;

@property (strong, nonatomic) NSString *selectedFontName;

@property (strong, nonatomic) NSArray *selectedFontMapArray;
@property (strong, nonatomic) NSArray *selectedFontDigitMapArray;

@property (strong, nonatomic) NSArray *normalFontMapArray;
@property (strong, nonatomic) NSArray *normalFontDigitMapArray;


- (void) setUpFancyFontObjectWithCompletionBlock:(setUpCompletionBlock)completionBlock;

- (BOOL) validateString:(NSString *) text;

- (void) modifyInputText:(NSString *)text;

@end