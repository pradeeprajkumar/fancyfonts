//
//  FancyFontsTextView.m
//  FancyFonts
//
//  Created by Pradeep Rajkumar on 30/12/14.
//  Copyright (c) 2014 FancyFonts. All rights reserved.
//

#import "FancyFontsTextView.h"

NSString * const kUpperCaseKey				= @"printHigh";
NSString * const kLowerCaseKey				= @"printLow";
NSString * const kNumberKey					= @"print";

@implementation FancyFontsTextView

-(void)setSelectedFontName:(NSString *)selectedFontName
{
	_selectedFontName = selectedFontName;
	
	self.selectedFontMapArray = [self updateFontDictionaryWithFontName:self.selectedFontName forMapType:@"map"];
	self.selectedFontDigitMapArray = [self updateFontDictionaryWithFontName:self.selectedFontName forMapType:@"digitMap"];
}

- (void) setUpFancyFontObjectWithCompletionBlock:(setUpCompletionBlock)completionBlock
{
	self.availableFontsArray = @[@"normal", @"bubs", @"crystal"];
	self.selectedFontName = [self.availableFontsArray objectAtIndex:0];
	self.selectedFontMapArray = [self updateFontDictionaryWithFontName:self.selectedFontName forMapType:@"map"];
	self.selectedFontDigitMapArray = [self updateFontDictionaryWithFontName:self.selectedFontName forMapType:@"digitMap"];
	self.normalFontMapArray = [self updateFontDictionaryWithFontName:self.selectedFontName forMapType:@"map"];
	self.normalFontDigitMapArray = [self updateFontDictionaryWithFontName:self.selectedFontName forMapType:@"digitMap"];
}

- (NSArray *) updateFontDictionaryWithFontName:(NSString *)fontName forMapType:(NSString *)mapType
{
	NSError *error = nil;
	NSMutableArray *fontSlotArray = [[NSMutableArray alloc]  init];
	
	NSMutableDictionary *fontDictionary = [[NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fontName ofType:@"json"]]
																		   options:kNilOptions
																			 error:&error] mutableCopy];
	if (error != nil)
	{
		NSLog(@"Error parsing the JSON file.");
	}
	
	fontSlotArray = [[fontSlotArray arrayByAddingObjectsFromArray:[fontDictionary objectForKey:mapType]] mutableCopy];
	return fontSlotArray;
}


- (int) getSlotForNormalFontValue:(NSString *)normalString forLetterType:(NSString *)letterType
{
	//Parse and find the slot number for the input value

	NSArray *filteredArray = [self filterArrayForLetterType:letterType forInputString:normalString];
	
	NSDictionary *filteredDictionary = [filteredArray lastObject];
	return (int)[[filteredDictionary objectForKey:@"slot"] integerValue];
}

- (NSArray *) filterArrayForLetterType:(NSString *)letterType forInputString:(NSString *)normalInputString
{
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K LIKE %@", letterType,normalInputString];
	NSArray *toBeFilteredArray;
	if ([letterType isEqualToString:kNumberKey])
	{
		toBeFilteredArray = self.normalFontDigitMapArray;
	}
	else
	{
		toBeFilteredArray = self.normalFontMapArray;
	}
	NSArray *filteredArray = [toBeFilteredArray filteredArrayUsingPredicate:pred];
	return filteredArray;
}

- (NSString *)getUnicodeCharacterForSlot:(int)slot forLetterType:(NSString *)letterType
{
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"slot == %d", slot];
	NSArray *filteredArr;
	if ([letterType isEqualToString:kNumberKey])
	{
		filteredArr = [self.selectedFontDigitMapArray filteredArrayUsingPredicate:pred];
	}
	else
	{
		filteredArr	= [self.selectedFontMapArray filteredArrayUsingPredicate:pred];
	}
	NSDictionary *filteredDictionary = [filteredArr lastObject];
	NSString *string = [filteredDictionary objectForKey:letterType];
	
	return string;
}

- (void) modifyInputText:(NSString *)text
{
	NSString *letterType = [self findLetterTypeForText:text];
	
	int slot = [self getSlotForNormalFontValue:text forLetterType:letterType];
	
	NSString *unicodeString = [self getUnicodeCharacterForSlot:slot forLetterType:letterType];
	
	[self insertText:unicodeString];
	NSLog(@"%@", unicodeString);
}

- (NSString *) findLetterTypeForText:(NSString *)text
{
	NSString *letterType;
	
	//Block to check whether the character is upper case or lower case
	BOOL (^isTextUppercase)(NSString *) = ^(NSString *string) {
		return [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[string characterAtIndex:0]];
	};
	
	//Block to check whether the character is a number
	BOOL (^isTextNumber)(NSString *) = ^(NSString *string) {
		NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
		if ([string rangeOfCharacterFromSet:notDigits].location == NSNotFound)
		{
			return YES;
		}
		else
		{
			return NO;
		}
	};

	if (isTextNumber(text))
	{
		letterType = kNumberKey;
	}
	else if (isTextUppercase(text))
	{
		letterType = kUpperCaseKey;
	}
	else
	{
		letterType = kLowerCaseKey;
	}

	return letterType;
}

- (BOOL) validateString:(NSString *) text
{
	if (text.length != 1)
	{
		return NO;
	}
	
	//Check for alpha numberic character validity
	NSCharacterSet *alphaSet = [NSCharacterSet alphanumericCharacterSet];
	BOOL stringValidity = [[text stringByTrimmingCharactersInSet:alphaSet] isEqualToString:@""];
	return stringValidity;
}

@end