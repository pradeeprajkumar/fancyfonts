//
//  ViewController.m
//  FancyFonts
//
//  Created by Pradeep Rajkumar on 30/12/14.
//  Copyright (c) 2014 FancyFonts. All rights reserved.
//

#import "FancyFontsViewController.h"
#import "FancyFontsTextView.h"
#import "FontsListTableViewCell.h"

@interface FancyFontsViewController ()

@property (weak, nonatomic) IBOutlet FancyFontsTextView *fancyFontsTextView;

@property (weak, nonatomic) IBOutlet UITableView *fontsListTableView;


- (IBAction)showFontsTableView:(id)sender;
- (IBAction)showKeyboard:(id)sender;

@end

NSString * const kFontTypeCellIdentifier	= @"FontTypeCell";

@implementation FancyFontsViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	__weak id weakTableView = self.fontsListTableView;
	[self.fancyFontsTextView setUpFancyFontObjectWithCompletionBlock:^(id responseData) {
		[weakTableView reloadData];
	}];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

- (IBAction)showFontsTableView:(id)sender
{
	[self.fancyFontsTextView resignFirstResponder];
}

- (IBAction)showKeyboard:(id)sender
{
	[self.fancyFontsTextView becomeFirstResponder];
}


#pragma mark - TableView Datasource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.fancyFontsTextView.availableFontsArray.count;
}

#pragma mark - TableView Delegate methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	FontsListTableViewCell *fontTypeCell = [tableView dequeueReusableCellWithIdentifier:kFontTypeCellIdentifier];
	[fontTypeCell.fontNameLabel setText:[self.fancyFontsTextView.availableFontsArray objectAtIndex:indexPath.row]];
	
	return fontTypeCell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.fancyFontsTextView.selectedFontName = [self.fancyFontsTextView.availableFontsArray objectAtIndex:indexPath.row];
}


#pragma mark - TextView Delegate methods
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if (![(FancyFontsTextView *)textView validateString:text])
	{
		return YES;
	}

	[(FancyFontsTextView *)textView modifyInputText:text];
	
	return NO;
}

- (IBAction)clearTextView:(id)sender
{
	[self.fancyFontsTextView setText:@"" ];
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	return YES;
}

@end