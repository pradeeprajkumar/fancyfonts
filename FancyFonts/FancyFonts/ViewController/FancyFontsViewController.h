//
//  ViewController.h
//  FancyFonts
//
//  Created by Pradeep Rajkumar on 30/12/14.
//  Copyright (c) 2014 FancyFonts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FancyFontsViewController : UIViewController<UITextViewDelegate, UITableViewDataSource, UITableViewDelegate>

@end